﻿using Lesson11.Task0;
using Lesson11.Task1;
using Lesson11.Task1.Goods;
using System.Collections.Generic;

namespace Lesson11
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*
                Задание 0:
                1. У вас есть следующая коллекция: 
                ArrayList list = new ArrayList();  
                И в вашей программе вызвается следующий код:
                object s = list[18];
                Необходимо перехватить ошибку и вывести на экран с указанием типа этой ошибки. 
                
                2. Необходимо создать коллекцию Dictionary, в которой будет находиться 10 различных пар объектов.
                Необходимо вывести содержимое коллекции на экран.
             */

            Task0_1.Example0_1();
            Task0_2.Example0_2();

            //Task1
            /*
                Задание 1
                Полную структуру классов и их взаимосвязь продумать самостоятельно.
                Исходные данные база (List) из n товаров задать непосредственно в коде (захардкодить).
                Создать базовый класс Product с методами, позволяющим вывести на экран информацию о товаре,
                а также определить, соответствует ли она сроку годности на текущую дату.
                Продукт (название, цена, дата производства, срок годности)
                
                Создать базу (List) из n товаров, вывести полную информацию из базы на экран,
                а также организовать поиск просроченного товара (на момент текущей даты).
            */

            //Задание 1.1
            // а - Создать List, содержащий объекты класса Product 
            List<Product> basket1 = new List<Product>()
            {
                {new Apple(new DateTime(2023, 8, 1)) },
                {new Cherry(new DateTime(2023, 9, 5)) },
                {new Cucumber(new DateTime(2023, 7, 20)) },
                {new Grape(new DateTime(2023, 8, 1)) },
                {new Lemon(new DateTime(2021, 1, 10)) },
                {new Melon(new DateTime(2023, 5, 25)) },
                {new Onion(new DateTime(2022, 12, 8)) },
                {new Orange(new DateTime(2023, 9, 24)) },
                {new Tomato(new DateTime(2023, 6, 15)) }
            };

            // б - Распечатать его содержимое используя foreach
            ShowBasket(basket1);

            // в - Изменить цену одного продукта на 100
            Console.WriteLine($"Name: {basket1[1].Name}, price: {basket1[1].Price}");
            basket1[1].Price = 100;
            Console.WriteLine($"Name: {basket1[1].Name}, price: {basket1[1].Price}");

            // г - Удалить последний продукт
            basket1.Remove(basket1[8]);

            // д - Распечатать его содержимое
            ShowBasket(basket1);

            // ж - Удалить все продукты
            basket1.Clear();

            /*
                Задание 1.2:
                Создать коллекцию, содержащую объекты Product.
                Написать метод, который перебирает элементы коллекции и проверяет цену продуктов.
                Если цена продукта больше 300 рублей, продукт перемещается в другую коллекцию.
                
                Необходимо вывести информацию о продукте у которого самая минимальна цена из полученной коллекции.
            */

            basket1 = new List<Product>()
            {
                {new Apple(new DateTime(2023, 8, 1)) },
                {new Cherry(new DateTime(2023, 9, 5)) },
                {new Cucumber(new DateTime(2023, 7, 20)) },
                {new Grape(new DateTime(2023, 8, 1)) },
                {new Lemon(new DateTime(2021, 1, 10)) },
                {new Melon(new DateTime(2023, 5, 25)) },
                {new Onion(new DateTime(2022, 12, 8)) },
                {new Orange(new DateTime(2023, 9, 24)) },
                {new Tomato(new DateTime(2023, 6, 15)) }
            };

            List<Product> basket2 = new List<Product>();
            foreach (var el in basket1)
            {
                if (el.Price > 300)
                {
                basket2.Add(el);
                }
            }

            SortingProductsByPrice(basket2);
            Console.WriteLine($"Name: {basket2[0].Name}, MinPrice: {basket2[0].Price}");

            /*
                Задание 1.3:
                Создайте Dictionary, содержащий пары значений -имя продукта и количестов единиц продукта например  Dictionary<string, int> { "Продукт 1", 5 },
                
                Перебрать и распечатать пары значений в формате "Name = abc, Count = 5"
                Перебрать и распечатать набор из имен продуктов
                Перебрать и распечатать значения количества единиц продуктов.
                Для каждого перебора создать свой метод.
            */

            Dictionary<string, int> basket3 = new Dictionary<string, int>()
            {
                {"apple", 8},
                {"tomato", 15},
                {"cucumber", 32},
                {"cherry", 200},
                {"lemon", 20},
                {"melon", 2},
                {"orange", 40},
            };

            ShowNameAndCount(basket3);
            ShowName(basket3);
            ShowCount(basket3);

            /*         
                На основе задания 1.3 необходимо написать преобразование 
            */

            //Dictionary в List.
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();

            foreach (var el in basket3)
            {
                list.Add(el);
            }

            //List в Dictionary.
            Dictionary<string, int> dictionary = new Dictionary<string, int>();

            foreach (var el in list)
            {
                dictionary.Add(el.Key, el.Value);
            }
        }

        public static void ShowNameAndCount<T1,T2>(Dictionary<T1,T2> basket)
        {
            foreach (var el in basket)
            {
                Console.WriteLine($"Name = {el.Key}, Count = {el.Value}");
            }
        }

        public static void ShowName<T1, T2>(Dictionary<T1, T2> basket)
        {
            foreach (var el in basket)
            {
                Console.WriteLine($"Name = {el.Key}");
            }
        }

        public static void ShowCount<T1, T2>(Dictionary<T1, T2> basket)
        {
            foreach (var el in basket)
            {
                Console.WriteLine($"Count = {el.Value}");
            }
        }


        public static void ShowBasket(List<Product> basket)
        {
            foreach (var el in basket)
            {
                el.ShowProductInfo();
                el.ShowProductExpirationDate();
            }
        }

        public static void SortingProductsByPrice(List<Product> list)
        {
            for (int i = list.Count - 1; i > 0; i--)   // на какое место будем ставим наибольший элемент
            {
                for (int j = 0; j < i; j++)   // проходим по не отсортированной последовательности
                {
                    if (list[j].Price > list[j + 1].Price)  // если порядок элементов неправильный
                    {
                        // меняем местами пары
                        var temp = list[j];
                        list[j] = list[j + 1];
                        list[j + 1] = temp;
                    }
                }
            }
        }
    }
}