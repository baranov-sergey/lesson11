﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson11.Task1
{
    /*
        Задание 1
        Полную структуру классов и их взаимосвязь продумать самостоятельно.
        Исходные данные база (List) из n товаров задать непосредственно в коде (захардкодить).
    */

    internal static class Store
    {
        public static List<string> Goods
        {
            get
            {
                return new List<string>
                {
                    "apple",
                    "tomato",
                    "cherry",
                    "cucumber",
                    "onion",
                    "lemon" ,
                    "orange",
                    "grape",
                    "melon"
                };
            }
        }
    }
}
