﻿
namespace Lesson11.Task1
{
    internal class Product
    {
        public string Name { get; }
        public uint Price { get; set; }
        public int ExpirationDate { get; }
        public DateTime DateOfManufacture { get; private set; }

        public Product(string name, uint price, int expirationDate, DateTime dateOfManufacture)
        {
            Name = name;
            Price = price;
            ExpirationDate = expirationDate;
            DateOfManufacture = dateOfManufacture;
        }

        public bool ShowProductExpirationDate()
        {
            if (DateTime.Now < DateOfManufacture.AddDays(ExpirationDate))
            {
                Console.WriteLine("Fresh product");
                return true;
            }
            else
            {
                Console.WriteLine("The product has expired");
                return false;
            }
        }

        public void ShowProductInfo()
        {
            Console.WriteLine($"Name product: {Name}, " +
                $"price: {Price}, expiration date: {ExpirationDate}, " +
                $"date of manufacture: {DateOfManufacture}");
        }
    }
}
