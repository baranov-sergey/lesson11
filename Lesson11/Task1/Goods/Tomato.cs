﻿
namespace Lesson11.Task1.Goods
{
    internal class Tomato : Product
    {
        public Tomato(DateTime dateOfManufacture) : base(Store.Goods[1], 350, 5, dateOfManufacture) { }
    }
}
