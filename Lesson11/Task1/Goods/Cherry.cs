﻿
namespace Lesson11.Task1.Goods
{
    internal class Cherry : Product
    {
        public Cherry(DateTime dateOfManufacture) : base(Store.Goods[2], 450, 10, dateOfManufacture) { }
    }
}
