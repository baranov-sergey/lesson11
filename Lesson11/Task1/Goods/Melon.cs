﻿
namespace Lesson11.Task1.Goods
{
    internal class Melon : Product
    {
        public Melon(DateTime dateOfManufacture) : base(Store.Goods[8], 310, 90, dateOfManufacture) { }
    }
}
