﻿
namespace Lesson11.Task1.Goods
{
    internal class Lemon : Product
    {
        public Lemon(DateTime dateOfManufacture) : base(Store.Goods[5], 200, 45, dateOfManufacture) { }
    }
}
