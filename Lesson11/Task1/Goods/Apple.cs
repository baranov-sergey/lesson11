﻿
namespace Lesson11.Task1.Goods
{
    internal class Apple : Product
    {
        public Apple(DateTime dateOfManufacture) : base(Store.Goods[0], 100, 14, dateOfManufacture) { }
    }
}
