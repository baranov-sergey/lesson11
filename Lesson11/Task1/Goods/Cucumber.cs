﻿
namespace Lesson11.Task1.Goods
{
    internal class Cucumber : Product
    {
        public Cucumber(DateTime dateOfManufacture) : base(Store.Goods[3], 100, 30, dateOfManufacture) { }
    }
}
