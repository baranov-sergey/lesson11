﻿
namespace Lesson11.Task1.Goods
{
    internal class Orange : Product
    {
        public Orange(DateTime dateOfManufacture) : base(Store.Goods[6], 150, 150, dateOfManufacture) { }
    }
}
