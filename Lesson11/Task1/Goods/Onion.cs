﻿
namespace Lesson11.Task1.Goods
{
    internal class Onion : Product
    {
        public Onion(DateTime dateOfManufacture) : base(Store.Goods[4], 50, 120, dateOfManufacture) { }
    }
}
