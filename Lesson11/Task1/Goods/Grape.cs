﻿
namespace Lesson11.Task1.Goods
{
    internal class Grape : Product
    {
        public Grape(DateTime dateOfManufacture) : base(Store.Goods[7], 400, 3, dateOfManufacture) { }
    }
}
