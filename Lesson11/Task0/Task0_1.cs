﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson11.Task0
{
    /*
        Задание 0:
        1. У вас есть следующая коллекция: 
        ArrayList list = new ArrayList();  
        И в вашей программе вызвается следующий код:
        object s = list[18];
        Необходимо перехватить ошибку и вывести на экран с указанием типа этой ошибки. 
     */

    internal static class Task0_1
    {
        public static void Example0_1()
        {
            ArrayList list = new ArrayList();
            try
            {
                object s = list[18];
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.WriteLine($"Type: {e.GetType()}\nMessage: {e.Message}");
            }
        }
    }
}
