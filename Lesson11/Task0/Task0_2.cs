﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson11.Task0
{
    /*
        2. Необходимо создать коллекцию Dictionary, в которой будет находиться 10 различных пар объектов.
        Необходимо вывести содержимое коллекции на экран.
    */

    internal class Task0_2
    {
        public static void Example0_2()
        {
            var dictionary = new Dictionary<int, string>()
            {
                {1,"apple" },
                {2,"tomat" },
                {3,"cherry" },
                {5,"cucmbers" },
                {6,"onion" },
                {7,"lemons" },
                {8,"orange" },
                {9,"grapes" },
                {10,"melon" }
            };

            foreach (var el in dictionary)
            {
                Console.WriteLine(el.Key + " " + el.Value);
            }
        }
    }
}
